from flask import Flask                                         # App Engine is going to wrap our Flask server in a production gunicorn server
from flask import render_template
from flask import request
from flask import jsonify
from flask import send_from_directory
from templates import *                                         # our 'templates' subdirectory
import re                                                       # regular expressions
import time                                                     # only used for '/ping' route
app = Flask(__name__)

@app.route('/')                                                 # a request for the main serve page (not a client's donation site)
def root():
    return render_template('index.html')

@app.route('/ping')                                             # a request for the main serve page (not a client's donation site)
def heartbeat():
    return time.ctime()                                         # ex: 'Mon Oct 18 13:35:29 2010'

@app.route('/js/<path:path>')                                   # all requests for javascript files
def send_js(path):
    return send_from_directory('templates/js', path)

@app.route('/css/<path:path>')                                  # all requests for css files
def send_css(path):
    return send_from_directory('templates/css', path)

@app.route('/favicon.*')                                        # all requests for a favicon
def send_favicon():
    return send_from_directory('templates/images', 'favicon.ico')

@app.route('/<path:path>')                                      # all other routes are treated as attempts to reach a client's donation page
def catch_all(path):
    return f'your path is: {path}'


if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080, debug=True)

