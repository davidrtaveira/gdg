import { Component } from '@angular/core';
import { Platform } from '@ionic/angular';
// import { SplashScreen } from '@ionic-native/splash-screen/ngx';
// import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Plugins } from '@capacitor/core';
import * as firebase from 'firebase/app';
import { firebaseConfig } from './credentials';

const { SplashScreen, StatusBar } = Plugins;

@Component({
    selector: 'app-root',
    templateUrl: 'app.component.html',
    styleUrls: ['app.component.scss']
})
export class AppComponent {
    constructor(
        private platform: Platform,
        // private splashScreen: SplashScreen,
        // private statusBar: StatusBar
    ) {
        this.initializeApp();
    }

    initializeApp() {
        firebase.initializeApp(firebaseConfig);
        
        this.platform.ready().then(() => {          // this promise is for when native hardware is ready to use
            // this.statusBar.styleDefault();
            // this.splashScreen.hide();

            SplashScreen.hide().catch(error => {
                console.error(error);
            });

            StatusBar.hide().catch(error => {
                console.error(error);
            });
        });
    }
}
