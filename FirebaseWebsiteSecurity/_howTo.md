NOTE: this uses parts of the 7-part tutorial here, https://javebratt.com/ionic-firebase-tutorial-intro/
specifically, it uses most of parts 1 & 2 with the logout from part 3

---

## Create a blank Ionic app

* ionic start exampleApp blank --type=angular
* cd exampleApp

---

## Remove @ionic-native packages

* npm rm @ionic-native/core @ionic-native/splash-screen @ionic-native/status-bar

* comment out (do not remove!) the @ionic-native references from
    `exampleApp > src > app > app.module.ts` and 
    `exampleApp > src > app > app.component.ts`

---

## Install the Firebase library

* npm install firebase --save

---

## Generate the Ionic pages

* ionic generate page pages/login
* ionic generate page pages/profile                 # this page will contain a logout button
* ionic generate page pages/reset-password
* ionic generate page pages/signup

---

## Generate the Auth service

* ionic generate service services/user/auth

---

## Enable Capacitor so we can talk to the phone hardware (for mobile phone users)

* ionic integrations enable capacitor
* change the `appId` line in `exampleApp > capacitor.config.json` to...

    "appId": "dev.cathedral.exampleApp"

    (this uses the nomenclature `com.myDomainName.myProjectName`)

* ionic build
* ionic cap add ios
  * if you get an error that `cocoapods is not installed`
  * first, you need to run this line from a Mac with XCode installed, then run...
  * sudo gem install cocoapods
* ionic cap add android
* ionic cap sync


* replace all of the @ionic-native references from `exampleApp > src > app > app.component.ts`
with references to Capacitor...

```
    import { Plugins } from '@capacitor/core';

    const { SplashScreen, StatusBar } = Plugins;

    initializeApp() {
        this.platform.ready().then(() => {          // this promise is for when native hardware is ready to use
            // this.statusBar.styleDefault();
            // this.splashScreen.hide();

            SplashScreen.hide().catch(error => {
                console.error(error);
            });

            StatusBar.hide().catch(error => {
                console.error(error);
            });
        });
    }
```

---

## Getting Firebase ready

go to: https://console.firebase.google.com

Create a Firebase app:
* Add Project
* (add a name)
* Create project (it doesn't matter if we add Google Analytics)
* Finish

Add a Firebase web app:
* click the 'web' icon that looks like "</>" rigth above to the text "Add an app to get started"
* choose an App Nickname in "Register app"
* do NOT check the "Also set up Firebase hosting..." button!
* click "Register app"
* copy all the code shown on the next page to a temp file (we'll come back to these credentials soon)
* click "continue to console"

Add login authentication:
* click 'Authentication' in the left menu column
* click the 'Sign-in method' tab
* click 'Email/Password' > 'enable' (don't enable 'passwordless signin') > Save

Add a cloud database:
* click 'Database' in the left menu column
* click 'Create database'
* choose 'start in test mode' > Next
* Cloud Firestore location > choose 'us-west2' > Done

Create a file to hold our Firebase app's credentials
* make the file `ExampleApp > src > app > credentials.ts`
* copy the temp file's `firebaseConfig` object into `credentials.ts` as

```
    export const firebaseConfig = {
        ...
    }
```
...we don't need the rest of the temp file's code

* add Firebase code to `exampleApp > src > app > app.component.ts`
```
    import * as firebase from 'firebase/app';
    import { firebaseConfig } from './credentials';

    initializeApp() {
        firebase.initializeApp(firebaseConfig);
        
        this.platform.ready().then(() => {
            ...
        }
    }
```

---

## At this point, we can run the app!

* command+t so we can open a new tab and keep writing commands
* ionic serve (in either tab)

---

## Secure the necessary pages (Authentication Guard)

* ionic generate guard services/user/auth
* add `imports` and the `CanActivate` code to `ExampleApp > src > app > services > user > auth.guard.ts`

* open `ExampleApp > src > app > app-routing.module.ts`
  * import `AuthGuard`
  * add the property `canActivate: [AuthGuard]` to any page that needs an authentication check

* open `ExampleApp > src > app > services > user > auth.service.ts` 
  * add imports
  * add the functions loginUser(), signupUser(), resetPassword(), and logoutUser()

---

## HTML and JavaScript for Login, Logout, Signup, and Password Reset

add the necessary code to the `ExampleApp > src > app > pages > *.page.html`, `*.page.ts`, and `*.page.scss` files for:
* login
* profile
* reset-password
* signup

for every one of these pages (login, profile, reset-password, and signup), go to the *.module.ts file...
* import `ReactiveFormsModule`
* add `ReactiveFormsModule` to the `@NgModule` import array




