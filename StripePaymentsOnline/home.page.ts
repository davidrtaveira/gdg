import { Component } from '@angular/core';
declare var Stripe;
// import { HttpClient } from "@angular/common/http";


@Component({
    selector: 'app-home',
    templateUrl: 'home.page.html',
    styleUrls: ['home.page.scss'],
})
export class HomePage {

    stripe = Stripe('pk_test_________________________________________');     // YOUR_STRIPE_PUBLISHABLE_KEY
    card: any;

    constructor(/*private http: HttpClient*/) { }

    ngOnInit() {
        this.setupStripe();
    }

    setupStripe() {
        let elements = this.stripe.elements();
        var cardStyle = {
            base: {
                color: '#32325d',
                lineHeight: '24px',
                fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '16px',
                '::placeholder': {
                    color: '#aab7c4'
                }
            },
            invalid: {
                color: '#fa755a',
                iconColor: '#fa755a'
            }
        };

        this.card = elements.create('card', { style: cardStyle });
        console.log(this.card);
        this.card.mount('#card-element');

        this.card.addEventListener('change', event => {
            var displayError = document.getElementById('card-errors');
            if (event.error) {
                displayError.textContent = event.error.message;
            } else {
                displayError.textContent = '';
            }
        });

        var form = document.getElementById('payment-form');
        form.addEventListener('submit', event => {
            event.preventDefault();
            console.log(event);

            ///////////////////////////////////
            this.createToken();

        });
    }

    stripeTokenHandler(token:any) {
        console.log('inside stripeTokenHandler!');

        // Insert the token ID into the form so it gets submitted to the server
        var form = document.getElementById('payment-form');
        var hiddenInput = document.createElement('input');
        hiddenInput.setAttribute('type', 'hidden');
        hiddenInput.setAttribute('name', 'stripeToken');
        hiddenInput.setAttribute('value', token.id);
        form.appendChild(hiddenInput);

        console.log('appending to hidden form element, token.id: ' + token.id);

        // Submit the form
        // form.submit();
    }

    test() {
        console.log('test!!!!!!!!!!!!');
    }

    createToken() {
        console.log('creating token!');
        var that = this;

        this.stripe.createToken(this.card).then(function(result) {
            console.log('result...');
            console.log(result);

            if (result.error) {
                // Inform the user if there was an error
                console.log('  result error');
                var errorElement = document.getElementById('card-errors');
                errorElement.textContent = result.error.message;
            } else {
                // Send the token to your server
                console.log('  no errors. trying to send the token to the Stripe server...');
                that.stripeTokenHandler(result.token);
                // that.test();
            }
        });
    }



}



