import { Injectable } from '@angular/core';
import * as firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';

import { Router } from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AuthService {

    constructor(private router: Router) { }

    loginUser(email: string, password: string): Promise<any> {
        console.log('auth service: loginUser...');
        
        return firebase.auth().signInWithEmailAndPassword(email, password)
            .then(() => {
                console.log('authService: loginUser: success');
            })
            .catch(error => {
                console.log('error.code...');
                console.log(error.code);
                console.log(error)
                if (error.code)
                    return false;
            });
    }

    logoutUser(): Promise<void> {
        return firebase.auth().signOut();
    }

    // signupUser(user: any): Promise<any> {
    //     console.log("auth");
    //     console.log(user);
    //     return firebase
    //         .auth()
    //         .createUserWithEmailAndPassword(user.email, user.password)
    //         .then((newUserCredential: firebase.auth.UserCredential) => {
    //             firebase
    //                 .firestore()
    //                 .doc(`/users/${newUserCredential.user.uid}`)
    //                 .set({ email: user.email }).then(() => {
    //                     console.log("user");
    //                     console.log(user);
    //                     this.setUserInfo(user);
    //                 });
    //         })
    //         .catch(error => {

    //             console.log('error.code...');
    //             console.log(error.code);
    //             console.log(error)
    //             if (error.code)
    //                 return false;
    //         });
    // }

    // resetPassword(email: string): Promise<any> {
    //     return firebase.auth().sendPasswordResetEmail(email)
    //         .then(() => {
    //             // this.router.navigate(['/amount']);
    //         })
    //         .catch(error => {
    //             console.log('error.code...');
    //             console.log(error.code);
    //             console.log(error)
    //             if (error.code)
    //                 return error;
    //         });
    // }
}
