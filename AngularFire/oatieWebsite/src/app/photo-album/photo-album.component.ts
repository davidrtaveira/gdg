import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';          // we include Auth here because we test if the user is logged in on the top navbar

import * as firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/storage';
import { ThrowStmt } from '@angular/compiler';

@Component({
    selector: 'app-photo-album',
    templateUrl: './photo-album.component.html',
    styleUrls: ['./photo-album.component.css']
})
export class PhotoAlbumComponent implements OnInit {

    robots: any[];                                              // stores all robots from our Cloud Firestore database
    currentRobot: any;                                          // stores 1 robot from our findRobot() call
    images: any[];                                              // stores links to all of our Firebase Storage images

    constructor(
        private authService: AuthService,
        private router: Router) {

        this.robots = [];
        this.findAllRobots();

        this.currentRobot = {};
        this.findRobot('bender@futurama.com');                  // find the robot with the given email (which we store as docID)

        this.images = [];
        this.getAllImagesFromStorage();
    }

    ngOnInit() {
    }

    logout() {
        this.authService.logoutUser().then(() => {
            console.log('logging out...');
            this.router.navigate(['/login']);
        }).catch(error => {
            console.log('logout error');
            console.log(error.code);
            console.log(error)
        });
    }

    findAllRobots() {
        let self = this;
        firebase.firestore().collection('robots').get()
            .then(function (querySnapshot) {
                querySnapshot.forEach(function (doc) {
                    console.log(doc.id, ' => ', doc.data());
                    self.robots.push(doc.data());
                });
            })
            .catch(function (error) {
                console.log('Error getting documents: ', error);
            });
    }

    findRobot(email) {
        let docID = email;                                      // find one doc where the email is the docID
        let self = this;
        firebase.firestore().collection('robots').doc(docID)
            .get()
            .then(function (doc) {
                console.log('findRobot...')
                console.log(doc.id, ' => ', doc.data());
                if (doc.data())
                    self.currentRobot = doc.data();
                else
                    self.currentRobot = {};
            })
            .catch(function (error) {
                console.log('Error getting document: ', error);
            });
    }


    getAllImagesFromStorage() {
        var self = this;
        var storageRef = firebase.storage().ref('kitten');      // get the images from the 'kitten' folder

        storageRef.listAll().then(function (result) {
            result.items.forEach(function (imageRef) {
                getImageDetails(imageRef);
            });
        }).catch(function (error) {
            console.log('getAllImagesFromStorage error:', error);
        });

        function getImageDetails(imageRef) {
            imageRef.getDownloadURL().then(function (url) {
                var imageDetails = {
                    bucket: imageRef['location']['bucket'],
                    path: imageRef['location']['path_'],
                    url: url
                };
                self.images.push(imageDetails);
            }).catch(function (error) {
                console.log('getAllImagesFromStorage: getImageDetails error:', error);
            });
        }
    }

}
