// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
    production: false,
    firebaseConfig: {
        apiKey: "AIzaSyDDf9ZOWtCHh9fj_Yg2CDHGiJ08HNGLUHY",
        authDomain: "oatiewebs.firebaseapp.com",
        databaseURL: "https://oatiewebs.firebaseio.com",
        projectId: "oatiewebs",
        storageBucket: "oatiewebs.appspot.com",
        messagingSenderId: "202308476602",
        appId: "1:202308476602:web:4155a57166ff53a3f2ac83"
    },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
